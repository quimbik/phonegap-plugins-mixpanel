var MixpanelPlugin = function() {
};

MixpanelPlugin.prototype.sharedInstanceWithToken = function(token)
{
    cordova.exec(null, null, "MixpanelPlugin", "sharedInstanceWithToken", [token]);
};

MixpanelPlugin.prototype.identify = function(username)
{
    cordova.exec(null, null, "MixpanelPlugin", "identify", [username]);
};

MixpanelPlugin.prototype.peopleSet = function(properties)
{
    cordova.exec(null, null, "MixpanelPlugin", "peopleSet", [properties]);
};

MixpanelPlugin.prototype.peopleIncrement = function(properties)
{
    cordova.exec(null, null, "MixpanelPlugin", "peopleIncrement", [properties]);
};

MixpanelPlugin.prototype.track = function(event, properties)
{
    if(typeof properties === "undefined"){
        properties = null;
    }
    cordova.exec(null, null, "MixpanelPlugin", "track", [event, properties]);
};

MixpanelPlugin.prototype.nameTag = function(name)
{
    cordova.exec(null, null, "MixpanelPlugin", "nameTag", [name]);
};

MixpanelPlugin.prototype.addPushDeviceToken = function(deviceToken)
{
    cordova.exec(null, null, "MixpanelPlugin", "addPushDeviceToken", [deviceToken]);
};

MixpanelPlugin.prototype.sendSenderId = function(senderId)
{
    cordova.exec(null, null, "MixpanelPlugin", "sendSenderId", [senderId]);
};

MixpanelPlugin.prototype.setPushRegistrationId = function(registrationId)
{
    cordova.exec(null, null, "MixpanelPlugin", "setPushRegistrationId", [registrationId]);
};

//-------------------------------------------------------------------

if(!window.plugins) {
    window.plugins = {};
}

if (!window.plugins.mixpanel) {
    window.plugins.mixpanel = new MixpanelPlugin();
}

if (module.exports) {
    module.exports = MixpanelPlugin;
}