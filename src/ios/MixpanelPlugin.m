//
//  MixpanelPlugin.m
//  Artkive
//
//  Created by Alessandro Jatoba on 4/3/13.
//
//

#import "MixpanelPlugin.h"
#import "Mixpanel.h"

@implementation MixpanelPlugin

NSData *CreateDataWithHexString(NSString *inputString)
{
    NSUInteger inLength = [inputString length];
    
    unichar *inCharacters = alloca(sizeof(unichar) * inLength);
    [inputString getCharacters:inCharacters range:NSMakeRange(0, inLength)];
    
    UInt8 *outBytes = malloc(sizeof(UInt8) * ((inLength / 2) + 1));
    
    NSInteger i, o = 0;
    UInt8 outByte = 0;
    for (i = 0; i < inLength; i++) {
        UInt8 c = inCharacters[i];
        SInt8 value = -1;
        
        if      (c >= '0' && c <= '9') value =      (c - '0');
        else if (c >= 'A' && c <= 'F') value = 10 + (c - 'A');
        else if (c >= 'a' && c <= 'f') value = 10 + (c - 'a');
        
        if (value >= 0) {
            if (i % 2 == 1) {
                outBytes[o++] = (outByte << 4) | value;
                outByte = 0;
            } else {
                outByte = value;
            }
            
        } else {
            if (o != 0) break;
        }
    }
    
    return [[NSData alloc] initWithBytesNoCopy:outBytes length:o freeWhenDone:YES];
}

- (void)sharedInstanceWithToken:(CDVInvokedUrlCommand*)command
{
    NSString* token = (NSString*)[command.arguments objectAtIndex:0];
    [Mixpanel sharedInstanceWithToken:token];
}

- (void)identify:(CDVInvokedUrlCommand*)command
{
    NSString* username = (NSString*)[command.arguments objectAtIndex:0];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel identify:username];
}

-(void)peopleSet:(CDVInvokedUrlCommand*)command
{
    NSDictionary* properties = (NSDictionary*)[command.arguments objectAtIndex:0];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people set:properties];
}

-(void)peopleIncrement:(CDVInvokedUrlCommand*)command
{
    NSDictionary* properties = (NSDictionary*)[command.arguments objectAtIndex:0];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people increment:properties];
}

-(void)track:(CDVInvokedUrlCommand*)command;
{
    NSString* event = (NSString*)[command.arguments objectAtIndex:0];
    NSDictionary* properties = (NSDictionary*)[command.arguments objectAtIndex:1];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    if([properties isKindOfClass:[NSNull class]]){
        [mixpanel track:event];
    } else {
        [mixpanel track:event properties:properties];
    }
}

-(void)nameTag:(CDVInvokedUrlCommand*)command;
{
    NSString* name = (NSString*)[command.arguments objectAtIndex:0];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];

    mixpanel.nameTag = name;
}

-(void)addPushDeviceToken:(CDVInvokedUrlCommand*)command;
{
    NSString* deviceToken = (NSString*)[command.arguments objectAtIndex:0];
    NSData* data = CreateDataWithHexString(deviceToken);
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel.people addPushDeviceToken:data];
}

@end
