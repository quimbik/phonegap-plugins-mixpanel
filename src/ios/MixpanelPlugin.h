//
//  MixpanelPlugin.h
//  Artkive
//
//  Created by Alessandro Jatoba on 4/3/13.
//
//

#import <Cordova/CDV.h>

@interface MixpanelPlugin : CDVPlugin

- (void)sharedInstanceWithToken:(CDVInvokedUrlCommand*)command;
- (void)identify:(CDVInvokedUrlCommand*)command;
- (void)peopleSet:(CDVInvokedUrlCommand*)command;
- (void)peopleIncrement:(CDVInvokedUrlCommand*)command;
- (void)track:(CDVInvokedUrlCommand*)command;
- (void)nameTag:(CDVInvokedUrlCommand*)command;
- (void)addPushDeviceToken:(CDVInvokedUrlCommand*)command;

@end