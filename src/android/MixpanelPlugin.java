package com.quimbik.phonegap.plugins.mixpanel;

import java.util.Iterator;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class MixpanelPlugin extends CordovaPlugin {

	private static MixpanelAPI mixpanel = null;

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
	    super.initialize(cordova, webView);
	    // your init code here
	}

	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		boolean result = false;

		if (action.equals("sharedInstanceWithToken")) {
			String token = args.getString(0);
			MixpanelPlugin.mixpanel = MixpanelAPI.getInstance(this.cordova.getActivity().getApplicationContext(), token);
		}

		
		if (action.equals("identify")) {
			String distinctId = args.getString(0);
			MixpanelPlugin.mixpanel.identify(distinctId);
			MixpanelPlugin.mixpanel.getPeople().identify(distinctId);
			result = true;
		} else if(action.equals("peopleSet")) {
			JSONObject properties = args.getJSONObject(0);
			MixpanelPlugin.mixpanel.getPeople().set(properties);
			result = true;
		} else if(action.equals("peopleIncrement")) {
			JSONObject properties = args.getJSONObject(0);
	        Iterator<String> keys = properties.keys();
	        while (keys.hasNext()){
	        	String key = keys.next();
	        	int value = properties.getInt(key);
	        	MixpanelPlugin.mixpanel.getPeople().increment(key, value);	        	
	        }
			result = true;
		} else if(action.equals("track")) {
			String event = args.getString(0);
			JSONObject properties = null;
			if(args.length() > 1){
				properties = args.getJSONObject(1);	
			}
			MixpanelPlugin.mixpanel.track(event, properties);
			result = true;
		} else if(action.equals("nameTag")) {
			/* The Android library doesn't support name tagging in the streams report directly, 
			 * but you can have a name there too by adding an mp_name_tag super property to your 
			 * events. 
			 */
			String name = args.getString(0);
			JSONObject nameTag = new JSONObject();
		    nameTag.put("mp_name_tag", name);
		    MixpanelPlugin.mixpanel.registerSuperProperties(nameTag);
			result = true;
		} else if(action.equals("sendSenderId")) {
			String senderId = args.getString(0);
			MixpanelPlugin.mixpanel.getPeople().initPushHandling(senderId);
			result = true;
		} else if(action.equals("setPushRegistrationId")){
			String registrationId = args.getString(0);
			MixpanelPlugin.mixpanel.getPeople().setPushRegistrationId(registrationId);
			result = true;
		}
		
		return result;
	}
	
	public static void flush() {
		MixpanelPlugin.mixpanel.flush();
	}

}
